import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './css/component.less';
import Log from 'react-log';
import Animate from 'rc-animate';
import MySubComponent from './MySubComponent';
import BasicExample from './BasicExample';
import ReactDOM from 'react-dom'
import {str,log} from './e1'
import rs from './e1'
// const VelocityComponent=require('velocity-animate');
import Transition from 'react-transition-group/Transition';
const duration = 300;
console.log(str)
console.log(rs())
console.log(log('rexz'))
class App extends Component {
  static propTypes={

  }

  constructor(props){
  	super(props);
  	this.state = {
      showSubComponent:1,
      show: false
    };
  }
  handleToggle() {
    this.setState(({ show }) => ({
      show: !show
    }))
  }

  componentDidUpdate(preProps) {//显示的时候执行动画
    if (this.props.show === true && preProps.show === false) {
      this.inAnimationTime = setTimeout(()=> {
        ReactDOM.findDOMNode(this).getElementsByClassName("modal")[0].classList.add("in");
      }, 50);
    }
  }

  componentDidMount(){
    var request = new Request('api/m/host/item-v3.8/46736', {
      method: 'GET'
    });
    fetch(request).then(function() { /* handle response */ });

  }
  render() {
    const { show } = this.state
    const LogExample = () => (
      <Log>
        <h1
          style={{
            color: 'black',
            fontSize: '50px',
            fontWeight: 'normal',
            fontFamily: 'Open Sans, sans-serif',
          }}>
          React
          <span
            style={{
              color: 'white',
              fontSize: '45px',
              fontWeight: 'bold',
              marginLeft: '10px',
              padding: '5px',
              fontFamily: 'Arial, Helvetica, sans-serif',
              background: 'linear-gradient(to bottom right, #13493b, #016a26)',
            }}>
            log
          </span>
        </h1>
      </Log>
    )



    const defaultStyle = {
      transition: `opacity ${duration}ms ease-in-out`,
      opacity: 0,
      padding: 20,
      display: 'inline-block',
      backgroundColor: '#8787d8'
    }

    const transitionStyles = {
      entering: { opacity: 0 },
      entered: { opacity: 1 },
    };
    //rest参数
    //当我们不确定函数参数的时候经常使用arguments对象，ES6引入rest参数概念可以不再使用该对象
    const Fade = ({ in: inProp }) => (
      <Transition in={inProp} timeout={duration}>
        {(state) => (
          <div style={{
            ...defaultStyle,
            ...transitionStyles[state]
          }}>
            I'm A fade Transition!
          </div>
        )}
      </Transition>
    );


    return (
      <div className="App">
      <BasicExample />
      <LogExample />
      <button onClick={() => this.handleToggle()}>
         Click to toggle
       </button>
      <div>
          <Fade in={!!show} />
        </div>
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
