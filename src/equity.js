import React, { Component } from 'react';
class equity extends Component {
  constructor(props) {
          super(props);
  }

   render(){
     return <li ng-repeat="item in memberFilterListContents.list" ng-class="{'is_used': (item.isUsed === 1)}">
					<a ng-click="openEquities(item)">
						<div class="left_pic" style="background-image: url('{{item.equitiesPic}}');background-repeat: no-repeat; background-position: center; background-size: cover;">
							<!-- <img y-lazy-src="{{item.equitiesPic}}" class="lazy_img" /> -->
							<i class="label_tag" ng-bind="item.equitiesLabel" ng-if="item.equitiesLabel"></i>
						</div>
						<div class="right_desc" ng-class="{'not_used': (item.isUsed === 0)}">
							<h3 class="title"><span class="equities-name-icon">赠</span>{{item.equitiesName}}</p></h3>
							<p class="price">价值<span class="price-tag">{{item.equitiesWorth}}</span><span class="price-unit">元</span></p>
							<p class="sub-title host-icon" ng-bind="item.hostName"></p>
							<div class="tags">
								<span ng-if="item.price" ng-bind="item.price"></span><span class="district" ng-class="{'w':!item.distance}"  ng-if="item.businessDistrict" ng-bind="item.businessDistrict"></span><span ng-if="item.distance" ng-bind="item.distance"></span></span>
							</div>
						</div>
					</a>
					<div class="bottom" ng-if="inAPP && item.isUsed === 0">
						<p class="one" ng-click="call(item.contactNumber)">立即预约</p>
						<p class="two">
							<span ng-if="!inAPP" ng-bind-html="[item.hostAddress, item.hostName, item.latitude, item.longitude] | yMap"></span>
							<span ng-if="inAPP"><a ng-click="navigate(item)" analytics-on="click" analytics-event="Vip-Host-Click" analytics-category="Map" analytics-label="{{item.hostId}}"></a></span> 导航前往
						</p>
						<p class="thr" ng-click="toShopAt($index, item)">确认品鉴</p>
					</div>
					<div class="bottom" ng-if="inAPP && item.isUsed === 1">
						<p class="share">
							<a ng-click="sharePic(item)"></a>晒图分享</p>
					</div>
				</li>
   }

}
export default equity;
