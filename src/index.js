import React from 'react';
import { render }  from 'react-dom';
import './index.css';
import App from './App';
import Draw from './Draw';
import Settings from './Settings';
import registerServiceWorker from './registerServiceWorker';
import { createStore } from 'redux';

import Counter from './components/Counter'
import counter from './reducers'


// import Counter from './components/Counter'
// import counter from './reducers'

import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
const isAuthed=321321321;
const store = createStore(counter);



const renders = () => render((
  <Router>


    <div>
    <Counter value={store.getState()} onIncrement=
    {()=>store.dispatch({type:'INCREMENT'})}
    onDecrement={()=>store.dispatch({type:'DECREMENT'})}/>

    <ul>
        <li><Link to="/">Form</Link></li>
        <li><Link to="/draw">draw</Link></li>
        <li><Link to="/settings">settings</Link></li>
      </ul>

    <Route  path="/" component={App}/>
    <Route path="/draw" component={Draw}/>
    <Route path='/settings' render={({ match }) => {
      return <Settings authed={isAuthed} match={match} />
    }} />
    </div>
  </Router>

), document.getElementById('root'))
renders()
store.subscribe(renders)

// ReactDOM.render(<App />, document.getElementById('root'));
//registerServiceWorker();
