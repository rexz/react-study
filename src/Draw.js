import React,{Component} from 'react';
import PropTypes from 'prop-types'
import logo from './logo.svg';
import './App.css';
import Child ,{str,f,Enhance,Enhance2}from './myComponent';
import GenderSelect from './GenderSelect';
import Hello from './Hello';



class Draw extends Component {
  constructor(props) {
    super(props);

    // 设置 initial state
    this.state = {
      text: 'placeholder',
      name:'',
      password:'',
      gender:'',
      val:0
    };

    // ES6 类中函数必须手动绑定
    this.handleChange = this.handleChange.bind(this);
    this.changeVal = this.changeVal.bind(this);
  }

  componentWillMount(){
    console.log('componentWillMount');
    console.log(str);
    console.log(f(4));
    console.log(this.state);
  }

  componentDidUpdate(){
    console.log('did update');
    console.log(this.state);


   }



   componentDidMount(){
    //  console.log(this.refs['child'].action1());
     this.setState({val: this.state.val + 1});
     console.log(this.state.val);
     console.log(this.props.data);


   }

  handleChange(event) {

    this.setState({text: 'REX',changeColor:true});
  }

  changeVal(event) {
    alert(2);
    this.setState({text: 'as'});
  }

  render() {




    const name="rex";


    function HelloComponent(props, /* context */) {
      var style={
        color:'red'
      }
      return <div style={style}>Hello {props.name}{props.status}</div>
    }
    return <div>



      <Hello isWarning="false"/>
      <Hello isWarning="true"/>
      <div onClick={this.changeVal}>clickme</div>
      <p>{this.state.text}</p>
      <HelloComponent name={this.state.text} status="blaaaaaaalalalalblal" changeColor={this.state.changeColor}/>
      {this.props.data}
      {this.props.workload}
      <Child ref="child" fullName={this.state.text} changeColor={this.state.changeColor} handleChange={this.handleChange}/>
    </div>
  }


}
export default Enhance2(Enhance(Draw));
Draw.propTypes={
  workload:PropTypes.number.isRequired
}
