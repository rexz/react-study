import React,{Component} from 'react';
import './css/component.css';
export default class Hello extends Component {
    constructor(props){
    	super(props);
    	this.state = {
        opacity: 1.0
      };
    }

    componentDidMount(){
      this.timer = setInterval(function () {
            var opacity = this.state.opacity;
            opacity -= .05;
            if (opacity < 0.1) {
              opacity = 1.0;
            }
            this.setState({
              opacity: opacity
            });
          }.bind(this), 100);
    }

    render() {
      var txt;

      if (this.props.isWarning==='false') {
        txt = <div style={{opacity:this.state.opacity}} className="warning">
          warning{this.props.isWarning}
        </div>;
      } else {
        txt = <div style={{opacity:this.state.opacity}} className="safe">
          safe{this.props.isWarning}
        </div>;
      }

        return (
          <div>{txt}</div>
        );
    }
}
