import React, { Component } from 'react';
import ReactDOM from 'react-dom';
class myComponent extends Component {
  constructor(props) {
      super(props);
      this.action2 = this.action2.bind(this);
      this.name=['周倜','周梅中','李桃云','周旋'];
      this.state={liked:true}
  }
  action1(){

  }
  action2(){
    alert('action2')
    this.setState({liked: !this.state.liked});
    ReactDOM.findDOMNode(this.refs.myTextInput).focus();
    //this.refs.myTextInput.focus();
  }
   render(){
     var divStyle={
       color:'blue',
       position:'absolute',
       display:'block',
       fontSize:'15px'
     }
     var nameList=this.name.map(function(name){
       return <p>Hello,{name}</p>
     })

     return <div style={{backgroundColor:this.props.changeColor?'#ddd':'red'}} onClick={this.props.handleChange}>xxxxx{this.props.fullName}
     <a style={divStyle} onClick={this.action2}>action2</a>{nameList}{this.state.liked?'喜欢':'不喜欢'}
     <input type="text" ref="myTextInput" />
               <input
                     type="button"
                     value="Focus the text input"
                     onClick={this.action2}
                   /></div>
   }

}
export default myComponent;
export const str="hello world";
export function f(a){
    return a+1
}
export const Enhance = (ComposedComponent) => class extends Component {
    constructor(props) {
        super(props);
        this.state = { data: 'MIKE' };
    }
    componentDidMount() {
        this.setState({ data: 'REXXXXXX' });
    }
    render() {
        return <ComposedComponent {...this.props} data={this.state.data} />;
    }
};

export const Enhance2 = (ComposedComponent) => class extends Component {
    constructor(props) {
        super(props);
        this.state = { workload: 2 };
    }
    componentDidMount() {
        this.setState({ workload: 3});
    }
    render() {
        return <ComposedComponent {...this.props} workload={this.state.workload} />;
    }
};
